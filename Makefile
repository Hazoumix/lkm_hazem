obj-m += my_module.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
	sudo insmod my_module.ko
	dmesg | tail

clean:
	sudo rmmod my_module
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean