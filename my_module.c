#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/timer.h>
#include <linux/mutex.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/string.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Hazem Bahloul");
MODULE_DESCRIPTION("My first linux kernel module");

/* ======= Define buffers' size ======= */
#define BUFFER_SIZE 256
#define MAX_WORDS 1000
#define WORD_SIZE 100

/* ======= Init Struct for data storage ======= */
struct my_data {
    char text[BUFFER_SIZE];
    char word_data[MAX_WORDS][WORD_SIZE];
    struct list_head list;
};

/* ======= List und mutix vars ======= */
LIST_HEAD(data_list);
DEFINE_MUTEX(list_mutex);
struct timer_list my_timer;

/* ======= cdev vars ======= */
static struct cdev my_cdev;
static dev_t dev;
static int major_num;
static int minor_num;

/* ======= Splitting und printing data vars ======= */
int i = 0;
int word_count = 0;

/* ======= Timed function for printing data ======= */
static void timer_callback(struct timer_list *t)
{
    //init struct pointer and temp struct
    struct my_data *data, *tmp;

    mutex_lock(&list_mutex);
    list_for_each_entry_safe(data, tmp, &data_list, list) {
        if (word_count > 0) {
            printk(KERN_INFO "my_module: printing_every_second Word %d: %s\n", i + 1, data->word_data[i]);
            i++;
            if (i == word_count){
                i = 0;
            }
        }
    }
    mutex_unlock(&list_mutex);
    //Reset timer
    mod_timer(&my_timer, jiffies + msecs_to_jiffies(1000));
}

/* ======= Timed function for printing data ======= */
static int my_module_uevent(struct device *dev, struct kobj_uevent_env *env)
{
    add_uevent_var(env, "DEVMODE=%#o", 0666);
    return 0;
}

/* ======= Add permissions to device file ======= */
static int my_module_open(struct inode *inode, struct file *file)
{
    printk(KERN_INFO "my_module: Device open\n");
    return 0;
}

/* ======= Function to run when device file is realeased ======= */
static int my_module_release(struct inode *inode, struct file *file)
{
    printk(KERN_INFO "my_module: Device close\n");
    
    return 0;
}

/* ======= Function to run when device file is read ======= */
static ssize_t my_module_read(struct file *file, char __user *buf, size_t count, loff_t *ppos)
{
    struct my_data *data, *tmp;
    char *tmp_buf;
    int copied = 0;

    printk(KERN_INFO "my_module: file readed\n");

    if (mutex_lock_interruptible(&list_mutex))
        return -ERESTARTSYS;

    list_for_each_entry_safe(data, tmp, &data_list, list) {
        printk(KERN_INFO "my_module: %d bytes sent to userspace\n", sizeof(tmp_buf));
        tmp_buf = data->text;
        while (*tmp_buf && (copied < count)) {
            if (put_user(*(tmp_buf++), buf + copied))
                break;
            copied++;
        }

        list_del(&data->list);
        kfree(data);

        if (copied >= count)
            break;
    }

    mutex_unlock(&list_mutex);
    return copied;
}

/* ======= Function to run when device file is writen ======= */
static ssize_t my_module_write(struct file *file, const char __user *buf, size_t count, loff_t *ppos)
{
struct my_data *data;

    char *word, *temp;;

    printk(KERN_INFO "my_module: file written\n");
    if (count >= BUFFER_SIZE)
        return -EINVAL;
    // Get data from user space and locate it into the memory
    data = kmalloc(sizeof(struct my_data), GFP_KERNEL);
    if (!data)
        return -ENOMEM;

    if (copy_from_user(data->text, buf, count)) {
        kfree(data);
        return -EFAULT;
    }
    printk(KERN_INFO "my_module: %d bytes recieved from userspace\n", sizeof(buf));

    // Split the data word-wise and save it into the data struct
    word_count = 0;
    temp = data->text;
    while ((word = strsep(&temp, " \t\n")) != NULL && word_count < MAX_WORDS) {
        strncpy(data->word_data[word_count], word, WORD_SIZE - 1);
        data->word_data[word_count][WORD_SIZE - 1] = '\0';
        word_count++;
    }

    mutex_lock(&list_mutex);
    list_add_tail(&data->list, &data_list);
    mutex_unlock(&list_mutex);

    return count;
}

/* ======= Function for device file IOCTL ======= */
static long my_module_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    printk(KERN_INFO "my_module: IOCTL\n");
    return 0;
}

/* ======= file Operations ======= */
static const struct file_operations my_module_fops = {
    .open       = my_module_open,
    .release    = my_module_release,
    .unlocked_ioctl = my_module_ioctl,
    .read       =  my_module_read,
    .write       = my_module_write
};

static struct class *my_class;
static struct device *my_device;

/* ======= function to init and register the module ======= */
static void create_char_device(void)
{
    int ret;

    ret = alloc_chrdev_region(&dev, 0, 1, "my_module");
    if (ret < 0) {
        pr_err("Failed to allocate char device region\n");
        return;
    }

    major_num = MAJOR(dev);
    minor_num = MINOR(dev);
    printk(KERN_INFO "my_module: LKM initialized (major=%d, minor=%d)\n", major_num, minor_num);

    my_class = class_create(THIS_MODULE, "my_module_class");
    if (IS_ERR(my_class)) {
        cdev_del(&my_cdev);
        unregister_chrdev_region(dev, 1);
        pr_err("Failed to create device class\n");
        return;
    }
    my_class->dev_uevent = my_module_uevent;

    cdev_init(&my_cdev, &my_module_fops);
    my_cdev.owner = THIS_MODULE;

    ret = cdev_add(&my_cdev, dev, 1);
    if (ret < 0) {
        unregister_chrdev_region(dev, 1);
        pr_err("Failed to add char device\n");
        return;
    }

    my_device = device_create(my_class, NULL, dev, NULL, "my_module");
    if (IS_ERR(my_device)) {
        class_destroy(my_class);
        cdev_del(&my_cdev);
        unregister_chrdev_region(dev, 1);
        pr_err("Failed to create device\n");
        return;
    }
}

/* ====== initialization function to call while start up ======= */
static int __init my_module_init(void)
{
    create_char_device();

    timer_setup(&my_timer, timer_callback, 0);
    mod_timer(&my_timer, jiffies + msecs_to_jiffies(1000));

    printk(KERN_INFO "my_module: LKM loaded\n");

    return 0;
}

/* ====== initialization function to call while exit ======= */
static void __exit my_module_exit(void)
{
    struct my_data *data, *tmp;

    del_timer_sync(&my_timer);

    mutex_lock(&list_mutex);
    list_for_each_entry_safe(data, tmp, &data_list, list) {
        list_del(&data->list);
        kfree(data);
    }
    mutex_unlock(&list_mutex);

    device_destroy(my_class, dev);
    class_unregister(my_class);
    class_destroy(my_class);

    cdev_del(&my_cdev);
    unregister_chrdev_region(dev, 1);

    printk(KERN_INFO "my_module: LKM unloaded\n");
}

/* ====== define module init and exit functions ======= */
module_init(my_module_init);
module_exit(my_module_exit);
